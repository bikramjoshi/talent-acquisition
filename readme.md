## Install dependencies:

```bash
npm install
```

## Database configuration

```bash
database : talent_acquisition
username : postgres
password : root
port: 5432
```

## Run database migration

```bash
npx sequelize-cli db:migrate
```

## Start the server:

```bash
node app.js or nodemon app.js
```

View the website at: http://localhost:3000/api/v1

View api documentation at: http://localhost:3000/api-docs/
