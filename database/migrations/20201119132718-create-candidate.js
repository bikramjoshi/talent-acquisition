'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('candidates', {
      candidate_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      email_id: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      phone_number: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      candidates_data: {
        type: Sequelize.JSONB,
        allowNull: true,
      },
      created_date: {
        type: Sequelize.DATE(3),
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)')
      },
      created_by: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      modified_date: {
        type: Sequelize.DATE(3),
        allowNull: true
      },
      modified_by: {
        type: Sequelize.STRING(100),
        allowNull: true,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('candidates');
  }
};