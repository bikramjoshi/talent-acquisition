const swaggerUi = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')

const jsDocOptions = {
    swaggerDefinition: {
        swagger: "2.0",
        info: {
            title: 'Talent Acquisition API',
            version: '1.0.0',
            description: 'API testing for Talent Acquisition app.'
        },
        basePath: '/api/v1',
    },
    apis: [
        './modules/Candidates/route.js'
    ]
};

var options = {
    swaggerOptions: {}
};

const swaggerSpec = swaggerJsDoc(jsDocOptions)

module.exports = (app) => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, options))
}