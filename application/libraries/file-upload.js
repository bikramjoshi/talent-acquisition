const multer = require('multer')
const fs = require('fs')
const path = require('path')

// SET STORAGE
var storage = multer.diskStorage({
    destination: function (req, file, cb) {

        var uploadDir = './uploads';

        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir);
        }

        cb(null, uploadDir)
    },
    filename: function (req, file, cb) {
        // cb(null, file.fieldname + '-' + Date.now())
        cb(null, file.fieldname + Date.now() + path.extname(file.originalname))
    }
})

// const upload = multer({ storage: storage })

// module.exports = upload;

module.exports = storage;