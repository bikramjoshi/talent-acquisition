'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Candidate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Candidate.init({
    candidate_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    name: DataTypes.STRING,
    email_id: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    candidates_data: DataTypes.JSONB,
    created_date: DataTypes.DATE(3),
    created_by: DataTypes.STRING,
    modified_date: DataTypes.DATE(3),
    modified_by: DataTypes.STRING
  }, {
    sequelize,
    tableName: 'candidates',
    modelName: 'Candidate',
    timestamps: false,
    // primaryKey: true
  });
  return Candidate;
};