const express = require('express')
const cors = require('cors')
const listEndpoints = require('express-list-endpoints')
const bodyParser = require('body-parser')
const dotenv = require('dotenv').config()

const swaggerDoc = require('./application/api/swagger/swaggerDoc')
const modules = require('./modules/modules')

const app = express();

const http = require('http').Server(app)

app.use(cors())
app.use(express.json())
app.use(express.static('public'))
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

swaggerDoc(app)

app.use(modules)

const port = process.env.PORT || 3000;

let server = http.listen(port, function () {
    if (process.env.NODE_ENV === 'development')
        console.log(listEndpoints(app));
});

module.exports = server;