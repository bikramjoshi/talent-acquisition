const router = require('express').Router();
require('express-router-group')

const CandidateModule = require('./Candidates/route')

// Module start
router.group("/api/v1", router => {
    router.use('/candidates', CandidateModule);
});
// Module end

module.exports = router;