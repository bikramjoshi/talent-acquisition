const router = require('express').Router();

const controller = require('./Controllers/Candidate')

/**
 * @swagger
 * /candidates:
 *   get:
 *     tags:
 *       - Candidates
 *     description: Get all candidates
 *     responses:
 *       200:
 *         description: Return list of candidates
 */
router.get('/', controller.list);

/**
 * @swagger
 * /candidates/{candidate_id}:
 *   get:
 *     tags:
 *       - Candidates
 *     description: Get a candidate by candidate id
 *     parameters:
 *      - in: path
 *        name: candidate_id
 *        required: true
 *        schema:
 *          type: integer
 *     responses:
 *       200:
 *         description: Return candidate by candidate id
 */
router.get('/:candidate_id', controller.find);

/**
 * @swagger
 * /candidates:
 *   post:
 *      tags:
 *       - Candidates
 *      description: Store candidate
 *      summary: Uploads a file.
 *      consumes:
 *       - multipart/form-data
 *      parameters:
 *       - in: formData
 *         name: candidates_excel
 *         type: file
 *         description: The file to upload.
 *      responses:
 *        200:
 *          description: Store candidate
 */
router.post('/', controller.store);

module.exports = router;