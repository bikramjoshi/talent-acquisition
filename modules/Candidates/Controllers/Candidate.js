const multer = require('multer')
const fs = require('fs')
const readXlsxFile = require('read-excel-file/node')
const Joi = require('joi');
const Sequelize = require('sequelize');

const db = require('../../../application/entities/index')
const storage = require('./../../../application/libraries/file-upload');

module.exports = {

    list(req, res) {
        return db.Candidate.findAll()
            .then(candidates => res.json({ data: candidates }))
            .catch(error => res.status(400).send(error));
    },

    find(req, res) {
        return db.Candidate.findByPk(req.params.candidate_id)
            .then(candidate => res.json({ data: candidate }))
            .catch(error => res.status(400).send(error));
    },

    store(req, res) {

        var upload = multer({ storage: storage }).any();

        upload(req, res, function (err) {

            if (err) res.status(400).end(err);

            if (req.files) {

                req.files.forEach(document => {

                    readXlsxFile(document.path).then(async (rows) => {

                        rows.shift();

                        let records = [];
                        let errors = [];
                        let recordExists = [];

                        rows.forEach(async (row, index, self) => {
                            module.exports._validateFields(row, errors);
                            module.exports._pushRecord(row, records);
                            module.exports._dataExists(row, index, recordExists);
                        });

                        return new Promise(
                            (resolve, reject) => {
                                setTimeout(() => resolve(recordExists), 1000);
                            })
                            .then((recordExists) => {

                                if (errors.length || recordExists.length) {

                                    module.exports._unlinkDocument(document.path)

                                    if (errors.length) {
                                        return res.status(400).send(errors)
                                    } else {
                                        return res.status(400).send({ "errors": recordExists })
                                    }
                                } else {

                                    module.exports._unlinkDocument(document.path)

                                    let requests = records.map((item) => {
                                        return new Promise((resolve, reject) => {
                                            db.Candidate.create(item)
                                                .then(data => resolve(data))
                                                .catch(error => reject(error));
                                        });
                                    })

                                    Promise.all(requests)
                                        .then((data) => res.json({ data: data, message: 'Candidates created successfully.' }))
                                        .catch(error => res.status(400).send(error));
                                }
                            });
                    });
                });
            }
        });
    },

    _candidateValidationSchema() {
        return Joi.object({
            name: Joi.string().required(),
            designation: Joi.string().required(),
            company_name: Joi.string().required(),
            experience: Joi.number().integer().required(),
            ctc_currency: Joi.string().valid('INR', 'USD', 'EUR'),
            ctc: Joi.number().integer().required(),
            ctc_type: Joi.string().when('ctc_currency', {
                is: 'INR',
                then: Joi.string().valid('LAKHS', 'CRORES'),
                otherwise: Joi.string().valid('THOUSANDS', 'MILLIONS')
            }),
            email_id: Joi.string().email(),
            contact_number: Joi.number().integer().required(),
            location: Joi.string().required(),
        }).options({ abortEarly: false });
    },

    _validateFields(row, errors) {

        const schema = module.exports._candidateValidationSchema();

        let e = schema.validate({
            name: row[0],
            designation: row[1],
            company_name: row[2],
            experience: row[3],
            ctc_currency: row[4],
            ctc: row[5],
            ctc_type: row[6],
            email_id: row[7],
            contact_number: row[8],
            location: row[10],
        });

        if (typeof e.error !== 'undefined') {
            errors.push(e.error.details);
        }
    },

    _pushRecord(row, records) {
        records.push({
            'name': row[0],
            'email_id': row[7],
            'phone_number': row[8],
            'candidates_data': {
                "ctc": {
                    "value": row[5],
                    "ctcUnit": row[6],
                    "ctcCurrency": row[4],
                },
                "candidateExperience": row[3],
                "company ": {
                    "name": row[2]
                },
                "location": {
                    "city": row[10]
                },
                "linkedIn": row[9]
            },
            'created_by': 'BIKRAM'
        });
    },

    async _dataExists(row, index, recordExists) {
        return await db.Candidate.count(
            {
                where: {
                    [Sequelize.Op.or]: [
                        { name: row[0] },
                        { phone_number: row[8].toString() },
                        { email_id: row[7] }
                    ]
                }
            })
            .then(count => {
                if (count != 0) {
                    return false;
                }
                return true;
            })
            .then(unique => {
                if (!unique) {
                    let column = [row[0], row[8], row[7]];
                    recordExists.push(`${index + 2} : ${column.join(" or ")} already exists in our records`)
                }
            })
            .catch(error => console.log(error));
    },

    _unlinkDocument(path) {
        // unlink document
        try {
            fs.unlinkSync(path)
        } catch (err) {
            console.error(err)
        }
    }
}